#TP4_Ejercicio2

import os 

def cargarProductos():
    productos = {}
    codigo = 'pass'
    print('Ingrese productos que desea registrar ')
    while codigo != 0:
        codigo = leerCodigo()
        if codigo not in productos and codigo > 0:
            descripcion = leerDescripcion()
            precio = leerPrecio()
            stock =  leerStock()
            productos[codigo] = [descripcion, precio, stock]
            print('**********Producto cargado**********')
        elif codigo == 0:
            pass
        else:
            print('El código ya existe')
    return productos

def leerCodigo():
    codigo = input('Ingrese codigo del producto (0 para finalizar registro): ')
    while codigo.isnumeric() == False:                
        codigo = input('Error. El codigo debe ser un entero positivo. Intente de nuevo: ')    
    return int(codigo)

def leerDescripcion():
    descripcion = input('Ingrese descripción del producto: ')
    while descripcion == '':            
        descripcion = input('Error. Ingresó espacio en blanco. Intente de nuevo: ')    
    return descripcion

def leerPrecio():
    precio = float(input('Ingrese precio del producto: '))
    while precio < 0:                
        precio = input('Error. El precio no puede ser negativo. Intente de nuevo: ') 
    return float(precio)

def leerStock():
    stock = input('Ingrese stock del producto: ')
    while stock.isnumeric() == False:                
        stock = input('Error. El stock debe ser un entero positivo. Intente de nuevo: ')    
    return int(stock)

def mostrarProductos(productos):
    print(productos)
    for codigo, datos in productos.items():
        print(codigo, datos)
        print(f'Código: {codigo} || Descripción: {datos[0]} || Precio: {datos[1]} || Stock: {datos[2]}')

def mostrarProductosIntervalo(productos):
    contador = 0
    print('Mostrar desde...')
    A = cargarNum()
    print('Hasta...')
    B = cargarNum()
    while A >= B:
        print('Error. El segundo número debe ser mayor que el primero. Intente de nuevo...')
        print('Mostrar desde...')
        A = cargarNum()
        print('Hasta...')
        B = cargarNum()

    for codigo, datos in productos.items():
        if A <= datos[2] <= B:
            print(f'Código: {codigo} || Descripción: {datos[0]} || Precio: {datos[1]} || Stock: {datos[2]}')
        else:
            contador += 1 
    if contador >= 1:
        print(f'Hay {contador} elementos que no pertenecen a este intervalo')

def sumarStock(productos):
    print('Cantidad X de stock a sumar')
    X = cargarNum()
    print('Cantidad máxima Y de stock de referencia')
    Y = cargarNum()
    for codigo, datos in productos.items():
        if datos[2] < Y:
            datos[2] =+ X
    print('Elementos añadidos')

def eliminarStock(productos):
    pos = 'pass'
    while pos != -1:
        pos = buscarStock0(productos)
        if pos == -1:
            print('No hay productos con stock igual a 0')
        else:
            del productos[pos]

def buscarStock0(productos):
    pos = -1
    for codigo, datos in productos.items():
        if datos[2] == 0:
            pos = codigo
            break
    return pos

#Funciones secundarias
def cargarNum():
    n = input('Ingrese un número: ')
    while n.isnumeric() == False:                
        n = input('Error. El número debe ser un entero positivo. Intente de nuevo: ')    
    return int(n)

def separador():
    print('*' * 50)

def presionarTecla():
    input('Presione ENTER para continuar...')

def menu():
    print('MENU:')
    print('Opción a: registrar productos')
    print('Opción b: mostrar listado de productos')
    print('Opción c: mostrar productos cuyo stock se encuentre en determinado intervalo')
    print('Opción d: sumar X al stock de todos los productos cuyo valor actual de stock sea menor al valor Y.')
    print('Opción e: eliminar todos los productos cuyo stock sea igual a cero')
    print('Opción f: fin del programa')
    separador()
    opc = input('Elija una opción: ')
    return opc

#Principal
os.system('cls')

productos_cargado = False
opc = 'pass'

while opc != 'f':
    opc = menu()
    if opc == 'a':
        productos = cargarProductos()
        productos_cargado = True
        presionarTecla()
        os.system('cls')
    elif opc == 'b':
        if productos_cargado == True:
            mostrarProductos(productos)
            presionarTecla()
            os.system('cls')
        else:
            print('Primero debe cargar el listado de productos')
            presionarTecla()
            os.system('cls')
    elif opc == 'c':
        if productos_cargado == True:
            mostrarProductosIntervalo(productos)
            presionarTecla()
            os.system('cls')
        else:
            print('Primero debe cargar el listado de productos')
            presionarTecla()
            os.system('cls')            
    elif opc == 'd':
        if productos_cargado == True:
            sumarStock(productos)
            presionarTecla()
            os.system('cls')
        else:
            print('Primero debe cargar el listado de productos')
            presionarTecla()
            os.system('cls') 
        os.system('cls')
    elif opc == 'e':
        if productos_cargado == True:
            eliminarStock(productos)
            presionarTecla()
            os.system('cls')
        else:
            print('Primero debe cargar el listado de productos')
            presionarTecla()
            os.system('cls')         
    elif opc == 'f':
        os.system('cls')
        print('FIN DEL PROGRAMA. ¡HASTA LUEGO!')
    else:
        print('¡Debe seleccionar una letra según las opciones! Intente nuevamente...')
        presionarTecla()
        os.system('cls')